<?php 

include 'conf.php';


// Eksekusi query pertama dengan alias pada nama kolom
$query1 = "SELECT timestamp AS timestamp_logs1, suhu AS suhu_logs1, kelembapan AS kelembapan_logs1, cahaya AS cahaya_logs1 FROM logs ORDER BY timestamp DESC LIMIT 10";
$result1 = $conn->query($query1);

// // Eksekusi query kedua dengan alias pada nama kolom
$query2 = "SELECT timestamp AS timestamp_logs2, suhu AS suhu_logs2, kelembapan AS kelembapan_logs2, cahaya AS cahaya_logs2 FROM logs2 ORDER BY timestamp DESC LIMIT 10";
$result2 = $conn->query($query2);

// // Eksekusi query ketiga dengan alias pada nama kolom
$query3 = "SELECT timestamp AS timestamp_logs3, suhu AS suhu_logs3, kelembapan AS kelembapan_logs3, cahaya AS cahaya_logs3 FROM logs3 ORDER BY timestamp DESC LIMIT 10";
$result3 = $conn->query($query3);

// Inisialisasi array $data
$data = array();

// Loop through the returned data from Query 1
while ($row = $result1->fetch_assoc()) {
    $data[] = $row;
}

// Loop through the returned data from Query 2
while ($row = $result2->fetch_assoc()) {
    $data[] = $row;
}

// Loop through the returned data from Query 3
while ($row = $result3->fetch_assoc()) {
    $data[] = $row;
}

// Now, $data contains the combined result from all three queries with different column names (aliases)
// You can use $data array to access the merged data from the three queries with the new column names (aliases)
$conn->close();
// Print the data for testing
print json_encode($data);

?>
