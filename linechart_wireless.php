<!DOCTYPE html>
<html>
<head>
   <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
   <!--<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>-->
   <script type="text/javascript" src="res/Chart.min.js"></script>
</head>
<body>
  <canvas id="linechartwireless" height="155"></canvas>

  <script>
    var chartConfig2 = {
      type: 'line',
      data: {
        labels: [],
        datasets: [
          // Dataset for each table (Node) will be added here
        ]
      },
      options: {
        scales: {
                y: {
                    beginAtZero: true,
                    max: 70 // Batasan nilai maksimal sumbu Y
                }
            },
        responsive: true,
        animation: {
          duration: 0 // Set the duration to 0 to disable animation
        }
      }
    };

    var ctx = document.getElementById('linechartwireless').getContext('2d');
    var LineGraph = new Chart(ctx, chartConfig2);

    function updateChartWireless() {
      $.ajax({
        url: "wireless_node.php",
        type: "GET",
        dataType: "json",
        success: function(data) {
          var logs1Data = [];
          var logs2Data = [];
          var logs3Data = [];

          data.forEach(function(item) {
            if (item.timestamp_logs1) {
              logs1Data.push(item);
            } else if (item.timestamp_logs2) {
              logs2Data.push(item);
            } else if (item.timestamp_logs3) {
              logs3Data.push(item);
            }
          });
      // Gabungkan data timestamp, suhu, kelembapan, dan cahaya untuk setiap tabel
      var labels_logs1 = logs1Data.map(function (item) { return item.timestamp_logs1; });
      var suhu_logs1 = logs1Data.map(function (item) { return parseFloat(item.suhu_logs1); });
      var kelembapan_logs1 = logs1Data.map(function (item) { return parseFloat(item.kelembapan_logs1); });
      var cahaya_logs1 = logs1Data.map(function (item) { return parseFloat(item.cahaya_logs1); });

      var labels_logs2 = logs2Data.map(function (item) { return item.timestamp_logs2; });
      var suhu_logs2 = logs2Data.map(function (item) { return parseFloat(item.suhu_logs2); });
      var kelembapan_logs2 = logs2Data.map(function (item) { return parseFloat(item.kelembapan_logs2); });
      var cahaya_logs2 = logs2Data.map(function (item) { return parseFloat(item.cahaya_logs2); });

      var labels_logs3 = logs3Data.map(function (item) { return item.timestamp_logs3; });
      var suhu_logs3 = logs3Data.map(function (item) { return parseFloat(item.suhu_logs3); });
      var kelembapan_logs3 = logs3Data.map(function (item) { return parseFloat(item.kelembapan_logs3); });
      var cahaya_logs3 = logs3Data.map(function (item) { return parseFloat(item.cahaya_logs3); });

      // Gabungkan data timestamp dari ketiga tabel menjadi satu array
      var labels_logs1 = logs1Data.map(function (item) { return item.timestamp_logs1; });
      var labels_logs2 = logs2Data.map(function (item) { return item.timestamp_logs2; });
      var labels_logs3 = logs3Data.map(function (item) { return item.timestamp_logs3; });

      var allTimestamps = labels_logs1.concat(labels_logs2, labels_logs3);

      // Hapus nilai yang duplikat dari array timestamps
      var uniqueTimestamps = allTimestamps.filter(function (value, index, self) {
        return self.indexOf(value) === index;
      });
          // Fill chart data
          chartConfig2.data.labels = uniqueTimestamps;

          chartConfig2.data.datasets = [
            {
              label: 'Suhu_node1',
              data: suhu_logs1,
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(52, 152, 219, 1)",
              borderColor: "rgba(52, 152, 219, 1)",
              pointHoverBackgroundColor: "rgba(52, 152, 219, 1)",
              pointHoverBorderColor: "rgba(52, 152, 219, 1)",
            },
            {
              label: 'Kelembapan_node1',
              data: kelembapan_logs1,
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(46, 204, 113, 1)",
              borderColor: "rgba(46, 204, 113, 1)",
              pointHoverBackgroundColor: "rgba(46, 204, 113, 1)",
              pointHoverBorderColor: "rgba(46, 204, 113, 1)"
            },
            {
              label: 'Cahaya_node1',
              data: cahaya_logs1,
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(231, 76, 60, 1)",
              borderColor: "rgba(231, 76, 60, 1)",
              pointHoverBackgroundColor: "rgba(231, 76, 60, 1)",
              pointHoverBorderColor: "rgba(231, 76, 60, 1)"
            },
            {
                  label: 'Suhu_node2',
                  data: suhu_logs2,
                  fill: false,
                  lineTension: 0.1,
                  backgroundColor: "rgba(155, 89, 182, 1)",
                  borderColor: "rgba(155, 89, 182, 1)",
                  pointHoverBackgroundColor: "rgba(155, 89, 182, 1)",
                  pointHoverBorderColor: "rgba(155, 89, 182, 1)",
                      },
                {
                  label: 'Kelembapan_node2',
                  data: kelembapan_logs2,
                  fill: false,
                  lineTension: 0.1,
                  backgroundColor: "rgba(230, 126, 34, 1)",
                  borderColor: "rgba(230, 126, 34, 1)",
                  pointHoverBackgroundColor: "rgba(230, 126, 34, 1)",
                  pointHoverBorderColor: "rgba(230, 126, 34, 1)"
                },
                {
                  label: 'Cahaya_node2',
                  data: cahaya_logs2,
                  fill: false,
                  lineTension: 0.1,
                  backgroundColor: "rgba(243, 156, 18, 1)",
                  borderColor: "rgba(243, 156, 18, 1)",
                  pointHoverBackgroundColor: "rgba(243, 156, 18, 1)",
                  pointHoverBorderColor: "rgba(243, 156, 18, 1)"
                },
                {
                  label: 'Suhu_node3',
                  data: suhu_logs3,
                  fill: false,
                  lineTension: 0.1,
                  backgroundColor: "rgba(233, 30, 99, 1)",
                  borderColor: "rgba(233, 30, 99, 1)",
                  pointHoverBackgroundColor: "rgba(233, 30, 99, 1)",
                  pointHoverBorderColor: "rgba(233, 30, 99, 1)",
                      },
                {
                  label: 'Kelembapan_node3',
                  data: kelembapan_logs3,
                  fill: false,
                  lineTension: 0.1,
                  backgroundColor: "rgba(211, 84, 0, 1)",
                  borderColor: "rgba(211, 84, 0, 1)",
                  pointHoverBackgroundColor: "rgba(211, 84, 0, 1)",
                  pointHoverBorderColor: "rgba(211, 84, 0, 1)"
                },
                {
                  label: 'Cahaya_node3',
                  data: cahaya_logs3,
                  fill: false,
                  lineTension: 0.1,
                  backgroundColor: "rgba(149, 165, 166, 1)",
                  borderColor: "rgba(149, 165, 166, 1)",
                  pointHoverBackgroundColor: "rgba(149, 165, 166, 1)",
                  pointHoverBorderColor: "rgba(149, 165, 166, 1)"
                }
          ];

          LineGraph.update();
        },
        error: function(xhr, status, error) {
          console.log(error);
        }
      });
    }

    $(document).ready(function() {
      // Call updateChart function when the page is first loaded
      updateChartWireless();

      // Call updateChart function every 5 seconds
      setInterval(updateChartWireless, 5000); // 5000 milliseconds = 5 seconds
    });
  </script>
</body>
</html>
