<?php 

include 'conf.php';

// Eksekusi query pertama dengan alias pada nama kolom
$query1 = "SELECT timestamp AS timestamp_eth1, suhu AS suhu_eth1, kelembapan AS kelembapan_eth1, cahaya AS cahaya_eth1 FROM ethernet ORDER BY timestamp DESC LIMIT 10";
$result1 = $conn->query($query1);

// Eksekusi query kedua dengan alias pada nama kolom
$query2 = "SELECT timestamp AS timestamp_eth2, suhu AS suhu_eth2, kelembapan AS kelembapan_eth2, cahaya AS cahaya_eth2 FROM ethernet2 ORDER BY timestamp DESC LIMIT 10";
$result2 = $conn->query($query2);

// Eksekusi query ketiga dengan alias pada nama kolom
$query3 = "SELECT timestamp AS timestamp_eth3, suhu AS suhu_eth3, kelembapan AS kelembapan_eth3, cahaya AS cahaya_eth3 FROM ethernet3 ORDER BY timestamp DESC LIMIT 10";
$result3 = $conn->query($query3);

// Inisialisasi array $data
$data = array();

// Loop through the returned data from Query 1
while ($row = $result1->fetch_assoc()) {
    $data[] = $row;
}

// Loop through the returned data from Query 2
while ($row = $result2->fetch_assoc()) {
    $data[] = $row;
}

// Loop through the returned data from Query 3
while ($row = $result3->fetch_assoc()) {
    $data[] = $row;
}

// Now, $data contains the combined result from all three queries with different column names (aliases)
// You can use $data array to access the merged data from the three queries with the new column names (aliases)
$conn->close();
// Print the data for testing
print json_encode($data);

?>
