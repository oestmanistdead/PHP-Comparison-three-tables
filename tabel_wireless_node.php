<?php include 'conf.php';?>

<!DOCTYPE html>
  <div class="col-md-2.5 p-2">
    <div class="card">
      <div class="card-header bg-primary" >
        <h3 class="card-title">Data Wireless Node 1</h3>
      </div>
      <div class="card-body p-0">
        <table class="table table-sm table-hover text-nowrap" id="Wireless-node-1-table">
          <thead>
            <tr>
              <th>No</th>
              <th><?include 'webfonts/suhu.svg';?></th>
              <th><?include 'webfonts/kelembapan.svg';?></th>
              <th><?include 'webfonts/cahaya.svg';?></th>
              <th><?include 'webfonts/waktu.svg';?></th>
            </tr>
          </thead>
          <tbody>
            <!-- Data will be added here through AJAX -->
          </tbody>
        </table>
      </div>
    </div>
  </div>



  <div class="col-md-2.5 p-2">
    <div class="card">
      <div class="card-header bg-warning">
        <h3 class="card-title">Data Wireless Node 2</h3>
      </div>
      <div class="card-body p-0">
        <table class="table table-sm table-hover text-nowrap" id="Wireless-node-2-table">
          <thead>
            <tr>
              <th>No</th>
              <th><?include 'webfonts/suhu.svg';?></th>
              <th><?include 'webfonts/kelembapan.svg';?></th>
              <th><?include 'webfonts/cahaya.svg';?></th>
              <th><?include 'webfonts/waktu.svg';?></th>
            </tr>
          </thead>
          <tbody>
            <!-- Data will be added here through AJAX -->
          </tbody>
        </table>
      </div>
    </div>
  </div>


  <div class="col-md-2.5 p-2">
    <div class="card">
      <div class="card-header bg-danger">
        <h3 class="card-title">Data Wireless Node 3</h3>
      </div>
      <div class="card-body p-0">
        <table class="table table-sm table-hover text-nowrap" id="Wireless-node-3-table">
          <thead>
            <tr>
              <th>No</th>
              <th><?include 'webfonts/suhu.svg';?></th>
              <th><?include 'webfonts/kelembapan.svg';?></th>
              <th><?include 'webfonts/cahaya.svg';?></th>
              <th><?include 'webfonts/waktu.svg';?></th>
            </tr>
          </thead>
          <tbody>
            <!-- Data will be added here through AJAX -->
          </tbody>
        </table>
      </div>
    </div>
  </div>





  <!-- Similar structure for Node 2 and Node 3... -->

  <script src="https://fina.penulisanilmiah.online/res/jquery.min.js"></script>
  <script>
    function updateTable2() {
      $.ajax({
        url: "https://fina.penulisanilmiah.online/wireless_node.php",
        type: "GET",
        dataType: "json",
        success: function(data) {
          var table_logs1 = [];
          var table_logs2 = [];
          var table_logs3 = [];

          data.forEach(function (item) {
        if (item.timestamp_logs1) {
          table_logs1.push(item);
        } else if (item.timestamp_logs2) {
          table_logs2.push(item);
        } else if (item.timestamp_logs3) {
          table_logs3.push(item);
        }
          });

// Gabungkan data timestamp, suhu, kelembapan, dan cahaya untuk setiap tabel
      var labels_logs1 = table_logs1.map(function (item) { return item.timestamp_logs1; });
      var suhu_logs1 = table_logs1.map(function (item) { return parseFloat(item.suhu_logs1); });
      var kelembapan_logs1 = table_logs1.map(function (item) { return parseFloat(item.kelembapan_logs1); });
      var cahaya_logs1 = table_logs1.map(function (item) { return parseFloat(item.cahaya_logs1); });

      var labels_logs2 = table_logs2.map(function (item) { return item.timestamp_logs2; });
      var suhu_logs2 = table_logs2.map(function (item) { return parseFloat(item.suhu_logs2); });
      var kelembapan_logs2 = table_logs2.map(function (item) { return parseFloat(item.kelembapan_logs2); });
      var cahaya_logs2 = table_logs2.map(function (item) { return parseFloat(item.cahaya_logs2); });

      var labels_logs3 = table_logs3.map(function (item) { return item.timestamp_logs3; });
      var suhu_logs3 = table_logs3.map(function (item) { return parseFloat(item.suhu_logs3); });
      var kelembapan_logs3 = table_logs3.map(function (item) { return parseFloat(item.kelembapan_logs3); });
      var cahaya_logs3 = table_logs3.map(function (item) { return parseFloat(item.cahaya_logs3); });



          var logs1TableBody = document.getElementById("Wireless-node-1-table").getElementsByTagName('tbody')[0];
          var logs2TableBody = document.getElementById("Wireless-node-2-table").getElementsByTagName('tbody')[0];
          var logs3TableBody = document.getElementById("Wireless-node-3-table").getElementsByTagName('tbody')[0];

         // Menghapus isi tabel sebelumnya (jika ada)
          logs1TableBody.innerHTML = "";
          logs2TableBody.innerHTML = "";
          logs3TableBody.innerHTML = "";

          // Menambahkan data logsernet 1 ke dalam tabel
          for (var i = 0; i < table_logs1.length; i++) {
            var row = logs1TableBody.insertRow(i);
            var cellNo = row.insertCell(0);
            var cellSuhu = row.insertCell(1);
            var cellKelembapan = row.insertCell(2);
            var cellCahaya = row.insertCell(3);
            var cellTimestamp = row.insertCell(4);

            cellNo.innerHTML = i + 1;
            cellSuhu.innerHTML = suhu_logs1[i];
            cellKelembapan.innerHTML = kelembapan_logs1[i];
            cellCahaya.innerHTML = cahaya_logs1[i];
            cellTimestamp.innerHTML = labels_logs1[i];
          }

          // Menambahkan data logsernet 2 ke dalam tabel
          for (var i = 0; i < table_logs2.length; i++) {
            var row = logs2TableBody.insertRow(i);
            var cellNo = row.insertCell(0);
            var cellSuhu = row.insertCell(1);
            var cellKelembapan = row.insertCell(2);
            var cellCahaya = row.insertCell(3);
            var cellTimestamp = row.insertCell(4);

            cellNo.innerHTML = i + 1;
            cellSuhu.innerHTML = suhu_logs2[i];
            cellKelembapan.innerHTML = kelembapan_logs2[i];
            cellCahaya.innerHTML = cahaya_logs2[i];
            cellTimestamp.innerHTML = labels_logs2[i];
          }

          // Menambahkan data logsernet 3 ke dalam tabel
          for (var i = 0; i < table_logs3.length; i++) {
            var row = logs3TableBody.insertRow(i);
            var cellNo = row.insertCell(0);
            var cellSuhu = row.insertCell(1);
            var cellKelembapan = row.insertCell(2);
            var cellCahaya = row.insertCell(3);
            var cellTimestamp = row.insertCell(4);

            cellNo.innerHTML = i + 1;
            cellSuhu.innerHTML = suhu_logs3[i];
            cellKelembapan.innerHTML = kelembapan_logs3[i];
            cellCahaya.innerHTML = cahaya_logs3[i];
            cellTimestamp.innerHTML = labels_logs3[i];
          }
        },
        error: function(xhr, status, error) {
          console.log(error);
        }
      });
    }

    $(document).ready(function() {
      // Initial update
      updateTable2();

      // Set interval to update the table every 5 seconds
      setInterval(updateTable2, 5000);
    });
  </script>