<?php include 'conf.php';
// session_start();

// // Cek apakah pengguna sudah login
// if (!isset($_SESSION['username'])) {
//     header("Location: login.php");
//     exit();
// }


?>
<html >
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <meta http-equiv="refresh" content="5"> -->
    <title>WSN greenhouse</title>
    <link rel="icon" href="icon.png" type="image/png">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0-11/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="res/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  </head>
  <style type="text/css">html {
      zoom: 85%;
    }
    .marquee {
            width: 100%;
            white-space: nowrap;
            overflow: hidden;
            box-sizing: border-box;
            animation: marquee 20s linear infinite;
        }

        @keyframes marquee {
            0% {
                transform: translateX(100%);
            }
            100% {
                transform: translateX(-100%);
            }
        }
    </style>
<body class="hold-transition sidebar-mini">

<div class="wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          
          <!-- /.col -->
          <? include 'tabel_ethernet_node.php'; ?>

           <div class="col-md-1 p-2">
           <table>
           <tr><th>
           <h3 class="card-title ">Info:</h3></th></tr>
           <tr ><th><? include 'webfonts/suhu.svg';?></th>
            <td>Suhu
             </td>
             </tr>
             <tr>
             <th><? include 'webfonts/kelembapan.svg';?></th>
             <td>Kelembapan</td> 
             </tr>
             <tr>
                <th><? include 'webfonts/cahaya.svg';?></th>
            <td>Cahaya</td>
             </tr>
             <tr>
               <th><?include 'webfonts/waktu.svg';?></th>
             <td>Waktu</td>
             </tr>
            </table>
            </div>
          
          <!-- /.coumn-->
          <div class="col-md-4 p-2">
            <!-- Line chart -->
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="far fa-chart-bar"></i>
                            Grafik pada 3 ethernet node
                        </h3>
                    </div>
              <!-- /.card-header -->
                    <div class="card-body">
                        <?php include'linechart_ethernet.php';?>
                    </div>
              <!-- /.card-body-->
                </div>
            <!-- /.card -->
            </div>
            <!-- /.col-md -->
        </div>
        <!-- /.row -->

        <div class="row">
          
          <?php include 'tabel_wireless_node.php'; ?>

        <div class="col-md-1 p-2">
           <table>
           <tr><th>
           <h3 class="card-title ">Info:</h3></th></tr>
            <tr ><th><?php include 'webfonts/suhu.svg';?></th>
            <td>Suhu
             </td>
             </tr>
             <tr>
             <th><? include 'webfonts/kelembapan.svg';?></th>
             <td>Kelembapan</td> 
             </tr>
             <tr>
                <th><? include 'webfonts/cahaya.svg';?></th>
            <td>Cahaya</td>
             </tr>
             <tr>
               <th><?include 'webfonts/waktu.svg';?></th>
             <td>Waktu</td>
             </tr>
            </table>
            </div>


          <div class="col-md-4 p-2">
            <!-- Line chart -->
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="far fa-chart-bar"></i>
                            Grafik pada 3 wireless node
                        </h3>
                    </div>
              <!-- /.card-header -->
                    <div class="card-body">
                        <?php  require'linechart_wireless.php';?>
                    </div>
              <!-- /.card-body-->
                </div>
            <!-- /.card -->
            </div>
            <!-- /.col-md -->
        </div>
        <!-- /.row -->
        </div>
        <!-- /.Row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 0.0.1
    </div>
    <strong>Copyright © 2023 <a href="https://fina.penulisanilmiah.online/">Ijoroyo.X.Kemangkress</a>.</strong> 
  </footer>

  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<!-- jQuery -->
<script src="res/jquery.min.js"></script>
<!-- jQuery UI -->
<script src="res/jquery-ui.min.js"></script>
<!-- Bootstrap 4 -->
<script src="res/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="res/adminlte.min.js"></script>
<!-- FLOT CHARTS -->
<script src="res/jquery.flot.js"></script>
<!-- Page script -->

</body>
</html>