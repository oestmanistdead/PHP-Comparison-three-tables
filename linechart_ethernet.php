<!DOCTYPE html>
<html>
<head>
   <script
   src="https://code.jquery.com/jquery-3.3.1.js"
   integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
   crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <!--<script type="text/javascript" src="res/Chart.min.js"></script>-->
</head>
<body>
  <canvas id="myChart1" height="155"></canvas>

  <script>
      
    $(document).ready(function(){
      function updateChartEthernet() {
      $.ajax({
        url : "ethernet_node.php",
        type : "GET",
        dataType: "json",
        success : function(data){

      var eth1Data = [];
      var eth2Data = [];
      var eth3Data = [];

      data.forEach(function (item) {
        if (item.timestamp_eth1) {
          eth1Data.push(item);
        } else if (item.timestamp_eth2) {
          eth2Data.push(item);
        } else if (item.timestamp_eth3) {
          eth3Data.push(item);
        }
      });

      // Gabungkan data timestamp, suhu, kelembapan, dan cahaya untuk setiap tabel
      var labels_eth1 = eth1Data.map(function (item) { return item.timestamp_eth1; });
      var suhu_eth1 = eth1Data.map(function (item) { return parseFloat(item.suhu_eth1); });
      var kelembapan_eth1 = eth1Data.map(function (item) { return parseFloat(item.kelembapan_eth1); });
      var cahaya_eth1 = eth1Data.map(function (item) { return parseFloat(item.cahaya_eth1); });

      var labels_eth2 = eth2Data.map(function (item) { return item.timestamp_eth2; });
      var suhu_eth2 = eth2Data.map(function (item) { return parseFloat(item.suhu_eth2); });
      var kelembapan_eth2 = eth2Data.map(function (item) { return parseFloat(item.kelembapan_eth2); });
      var cahaya_eth2 = eth2Data.map(function (item) { return parseFloat(item.cahaya_eth2); });

      var labels_eth3 = eth3Data.map(function (item) { return item.timestamp_eth3; });
      var suhu_eth3 = eth3Data.map(function (item) { return parseFloat(item.suhu_eth3); });
      var kelembapan_eth3 = eth3Data.map(function (item) { return parseFloat(item.kelembapan_eth3); });
      var cahaya_eth3 = eth3Data.map(function (item) { return parseFloat(item.cahaya_eth3); });

      // Gabungkan data timestamp dari ketiga tabel menjadi satu array
      var labels_eth1 = eth1Data.map(function (item) { return item.timestamp_eth1; });
      var labels_eth2 = eth2Data.map(function (item) { return item.timestamp_eth2; });
      var labels_eth3 = eth3Data.map(function (item) { return item.timestamp_eth3; });

      var allTimestamps = labels_eth1.concat(labels_eth2, labels_eth3);

      // Hapus nilai yang duplikat dari array timestamps
      var uniqueTimestamps = allTimestamps.filter(function (value, index, self) {
        return self.indexOf(value) === index;
      });
      
          // Konfigurasi chart
          var chartConfig1 = {
            type: 'line',
            data: {
              labels: uniqueTimestamps,
              datasets: [
                {
                  label: 'Suhu_node1',
                  data: suhu_eth1,
                  fill: false,
                  lineTension: 0.1,
                  backgroundColor: "rgba(59, 89, 152, 0.75)",
                  borderColor: "rgba(59, 89, 152, 1)",
                  pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
                  pointHoverBorderColor: "rgba(59, 89, 152, 1)",
                      },
                {
                  label: 'Kelembapan_node1',
                  data: kelembapan_eth1,
                  fill: false,
                  lineTension: 0.1,
                  backgroundColor: "rgba(29, 202, 255, 0.75)",
                  borderColor: "rgba(29, 202, 255, 1)",
                  pointHoverBackgroundColor: "rgba(29, 202, 255, 1)",
                  pointHoverBorderColor: "rgba(29, 202, 255, 1)"
                },
                {
                  label: 'Cahaya_node1',
                  data: cahaya_eth1,
                  fill: false,
                  lineTension: 0.1,
                  backgroundColor: "rgba(211, 72, 54, 0.75)",
                  borderColor: "rgba(211, 72, 54, 1)",
                  pointHoverBackgroundColor: "rgba(211, 72, 54, 1)",
                  pointHoverBorderColor: "rgba(211, 72, 54, 1)"
                },
                {
                  label: 'Suhu_node2',
                  data: suhu_eth2,
                  fill: false,
                  lineTension: 0.1,
                  backgroundColor: "rgba(0, 250, 81, 0.8)",
                  borderColor: "rgba(0, 250, 81, 0.8)",
                  pointHoverBackgroundColor: "rgba(0, 250, 81, 0.8)",
                  pointHoverBorderColor: "rgba(0, 250, 81, 0.8)",
                      },
                {
                  label: 'Kelembapan_node2',
                  data: kelembapan_eth2,
                  fill: false,
                  lineTension: 0.1,
                  backgroundColor: "rgba(0, 166, 54, 0.8)",
                  borderColor: "rgba(0, 166, 54, 0.8)",
                  pointHoverBackgroundColor: "rgba(0, 166, 54, 0.8)",
                  pointHoverBorderColor: "rgba(0, 166, 54, 0.8)"
                },
                {
                  label: 'Cahaya_node2',
                  data: cahaya_eth2,
                  fill: false,
                  lineTension: 0.1,
                  backgroundColor: "rgba(0, 99, 32, 0.8)",
                  borderColor: "rgba(0, 99, 32, 0.8)",
                  pointHoverBackgroundColor: "rgba(0, 99, 32, 0.8)",
                  pointHoverBorderColor: "rgba(0, 99, 32, 0.8)"
                },
                {
                  label: 'Suhu_node3',
                  data: suhu_eth3,
                  fill: false,
                  lineTension: 0.1,
                  backgroundColor: "rgba(59, 89, 152, 0.75)",
                  borderColor: "rgba(59, 89, 152, 1)",
                  pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
                  pointHoverBorderColor: "rgba(59, 89, 152, 1)",
                      },
                {
                  label: 'Kelembapan_node3',
                  data: kelembapan_eth3,
                  fill: false,
                  lineTension: 0.1,
                  backgroundColor: "rgba(29, 202, 255, 0.75)",
                  borderColor: "rgba(29, 202, 255, 1)",
                  pointHoverBackgroundColor: "rgba(29, 202, 255, 1)",
                  pointHoverBorderColor: "rgba(29, 202, 255, 1)"
                },
                {
                  label: 'Cahaya_node3',
                  data: cahaya_eth3,
                  fill: false,
                  lineTension: 0.1,
                  backgroundColor: "rgba(211, 72, 54, 0.75)",
                  borderColor: "rgba(211, 72, 54, 1)",
                  pointHoverBackgroundColor: "rgba(211, 72, 54, 1)",
                  pointHoverBorderColor: "rgba(211, 72, 54, 1)"
                }
              ]
            },
            options: {
                scales: {
                y: {
                    beginAtZero: true,
                    min: 0,
                    max: 70 // Batasan nilai maksimal sumbu Y
                }
            },
              responsive: true,
              animation:{
                  duration: 0
              }
              
            }
          };

          // Menggambar chart
          var ctx = document.getElementById('myChart1').getContext('2d');
          new Chart(ctx, chartConfig1);
        },
        error : function(xhr, status, error) {
          console.log(error);
        }
      });
    }
      updateChartEthernet();
      setInterval(updateChartEthernet, 5000);
    });
  </script>
</body>
</html>
