<?php include 'conf.php';?>

<!DOCTYPE html>
 <!-- Table for Node 1 -->
  <div class="col-md-2.5 p-2">
    <div class="card">
      <div class="card-header bg-primary" >
        <h3 class="card-title">Data Wire Node 1</h3>
      </div>
      <div class="card-body p-0">
        <table class="table table-sm table-hover text-nowrap" id="node1-table">
          <thead>
            <tr>
              <th>No</th>
              <th><?include 'webfonts/suhu.svg';?></th>
              <th><?include 'webfonts/kelembapan.svg';?></th>
              <th><?include 'webfonts/cahaya.svg';?></th>
              <th><?include 'webfonts/waktu.svg';?></th>
            </tr>
          </thead>
          <tbody>
            <!-- Data will be added here through AJAX -->
          </tbody>
        </table>
      </div>
    </div>
  </div>



  <div class="col-md-2.5 p-2">
    <div class="card">
      <div class="card-header bg-warning">
        <h3 class="card-title">Data Wire Node 2</h3>
      </div>
      <div class="card-body p-0">
        <table class="table table-sm table-hover text-nowrap" id="node2-table">
          <thead>
            <tr>
              <th>No</th>
              <th><?include 'webfonts/suhu.svg';?></th>
              <th><?include 'webfonts/kelembapan.svg';?></th>
              <th><?include 'webfonts/cahaya.svg';?></th>
              <th><?include 'webfonts/waktu.svg';?></th>
            </tr>
          </thead>
          <tbody>
            <!-- Data will be added here through AJAX -->
          </tbody>
        </table>
      </div>
    </div>
  </div>


  <div class="col-md-2.5 p-2">
    <div class="card">
      <div class="card-header bg-danger">
        <h3 class="card-title">Data Wire Node 3</h3>
      </div>
      <div class="card-body p-0">
        <table class="table table-sm table-hover text-nowrap" id="node3-table">
          <thead>
            <tr>
              <th>No</th>
              <th><?include 'webfonts/suhu.svg';?></th>
              <th><?include 'webfonts/kelembapan.svg';?></th>
              <th><?include 'webfonts/cahaya.svg';?></th>
              <th><?include 'webfonts/waktu.svg';?></th>
            </tr>
          </thead>
          <tbody>
            <!-- Data will be added here through AJAX -->
          </tbody>
        </table>
      </div>
    </div>
  </div>





  <!-- Similar structure for Node 2 and Node 3... -->

  <script src="https://fina.penulisanilmiah.online/res/jquery.min.js"></script>
  <script>
    function updateTable() {
      $.ajax({
        url: "https://fina.penulisanilmiah.online/ethernet_node.php",
        type: "GET",
        dataType: "json",
        success: function(data) {
          var table_eth1 = [];
          var table_eth2 = [];
          var table_eth3 = [];

          data.forEach(function (item) {
        if (item.timestamp_eth1) {
          table_eth1.push(item);
        } else if (item.timestamp_eth2) {
          table_eth2.push(item);
        } else if (item.timestamp_eth3) {
          table_eth3.push(item);
        }
          });

// Gabungkan data timestamp, suhu, kelembapan, dan cahaya untuk setiap tabel
      var labels_eth1 = table_eth1.map(function (item) { return item.timestamp_eth1; });
      var suhu_eth1 = table_eth1.map(function (item) { return parseFloat(item.suhu_eth1); });
      var kelembapan_eth1 = table_eth1.map(function (item) { return parseFloat(item.kelembapan_eth1); });
      var cahaya_eth1 = table_eth1.map(function (item) { return parseFloat(item.cahaya_eth1); });

      var labels_eth2 = table_eth2.map(function (item) { return item.timestamp_eth2; });
      var suhu_eth2 = table_eth2.map(function (item) { return parseFloat(item.suhu_eth2); });
      var kelembapan_eth2 = table_eth2.map(function (item) { return parseFloat(item.kelembapan_eth2); });
      var cahaya_eth2 = table_eth2.map(function (item) { return parseFloat(item.cahaya_eth2); });

      var labels_eth3 = table_eth3.map(function (item) { return item.timestamp_eth3; });
      var suhu_eth3 = table_eth3.map(function (item) { return parseFloat(item.suhu_eth3); });
      var kelembapan_eth3 = table_eth3.map(function (item) { return parseFloat(item.kelembapan_eth3); });
      var cahaya_eth3 = table_eth3.map(function (item) { return parseFloat(item.cahaya_eth3); });



          var eth1TableBody = document.getElementById("node1-table").getElementsByTagName('tbody')[0];
          var eth2TableBody = document.getElementById("node2-table").getElementsByTagName('tbody')[0];
          var eth3TableBody = document.getElementById("node3-table").getElementsByTagName('tbody')[0];

         // Menghapus isi tabel sebelumnya (jika ada)
          eth1TableBody.innerHTML = "";
          eth2TableBody.innerHTML = "";
          eth3TableBody.innerHTML = "";

          // Menambahkan data ethernet 1 ke dalam tabel
          for (var i = 0; i < table_eth1.length; i++) {
            var row = eth1TableBody.insertRow(i);
            var cellNo = row.insertCell(0);
            var cellSuhu = row.insertCell(1);
            var cellKelembapan = row.insertCell(2);
            var cellCahaya = row.insertCell(3);
            var cellTimestamp = row.insertCell(4);

            cellNo.innerHTML = i + 1;
            cellSuhu.innerHTML = suhu_eth1[i];
            cellKelembapan.innerHTML = kelembapan_eth1[i];
            cellCahaya.innerHTML = cahaya_eth1[i];
            cellTimestamp.innerHTML = labels_eth1[i];
          }

          // Menambahkan data ethernet 2 ke dalam tabel
          for (var i = 0; i < table_eth2.length; i++) {
            var row = eth2TableBody.insertRow(i);
            var cellNo = row.insertCell(0);
            var cellSuhu = row.insertCell(1);
            var cellKelembapan = row.insertCell(2);
            var cellCahaya = row.insertCell(3);
            var cellTimestamp = row.insertCell(4);

            cellNo.innerHTML = i + 1;
            cellSuhu.innerHTML = suhu_eth2[i];
            cellKelembapan.innerHTML = kelembapan_eth2[i];
            cellCahaya.innerHTML = cahaya_eth2[i];
            cellTimestamp.innerHTML = labels_eth2[i];
          }

          // Menambahkan data ethernet 3 ke dalam tabel
          for (var i = 0; i < table_eth3.length; i++) {
            var row = eth3TableBody.insertRow(i);
            var cellNo = row.insertCell(0);
            var cellSuhu = row.insertCell(1);
            var cellKelembapan = row.insertCell(2);
            var cellCahaya = row.insertCell(3);
            var cellTimestamp = row.insertCell(4);

            cellNo.innerHTML = i + 1;
            cellSuhu.innerHTML = suhu_eth3[i];
            cellKelembapan.innerHTML = kelembapan_eth3[i];
            cellCahaya.innerHTML = cahaya_eth3[i];
            cellTimestamp.innerHTML = labels_eth3[i];
          }
        },
        error: function(xhr, status, error) {
          console.log(error);
        }
      });
    }

    $(document).ready(function() {
      // Initial update
      updateTable();

      // Set interval to update the table every 5 seconds
      setInterval(updateTable, 5000);
    });
  </script>